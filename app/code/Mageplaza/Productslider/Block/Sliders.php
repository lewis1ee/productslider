<?php


namespace Mageplaza\Productslider\Block;


use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Cms\Block\Block;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Url\EncoderInterface;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory as BestSellersCollectionFactory;
use Mageplaza\Productslider\Helper\Data;

//extends magento block class
//use factory/objmanager of classes needed
//->setData()->toHtml();
class Sliders extends Block
{
    protected $context;
    protected $dateTime;
    protected $bestSellersCollectionFactory;
    protected $productCollectionFactory;
    protected $catalogProductVisibility;
    protected $helperData;
    protected $categoryFactory;
    protected $httpContext;
    protected $urlEncoder;


    /**
     *
     * @param Context $context
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param DateTime $dateTime
     * @param Data $helperData
     * @param HttpContext $httpContext
     * @param EncoderInterface $urlEncoder
     * @param CategoryFactory $categoryFactory
     * @param BestSellersCollectionFactory $bestSellersCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Mageplaza\Productslider\Block\CustomProducts $customProducts
        Context $context,
        CollectionFactory $productCollectionFactory,
        Visibility $catalogProductVisibility,
        DateTime $dateTime,
        Data $helperData,
        HttpContext $httpContext,
        EncoderInterface $urlEncoder,
        CategoryFactory $categoryFactory,
        BestSellersCollectionFactory $bestSellersCollectionFactory,
        array $data = []
    ) {
        $this->context = $context;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->dateTime = $dateTime;
        $this->helperData = $helperData;
        $this->httpContext = $httpContext;
        $this->urlEncoder = $urlEncoder;
        $this->categoryFactory = $categoryFactory;
        $this->bestSellersCollectionFactory = $bestSellersCollectionFactory;

        parent::__construct(
            $context,
            $productCollectionFactory,
            $catalogProductVisibility,
            $dateTime,
            $helperData,
            $httpContext,
            $urlEncoder,
            $data
        );
    }

    public function getProductCollection()
    {
        switch ($this->getCategory()):
            case "new": return new NewProductsFactory($this->context,
                $this->productCollectionFactory, $this->catalogProductVisibility, $this->dateTime,
                $this->helperData, $this->httpContext, $this->urlEncoder);
            case "best-seller": return new BestSellerProducts($this->context,
                $this->productCollectionFactory, $this->catalogProductVisibility, $this->dateTime,
                $this->helperData, $this->httpContext, $this->urlEncoder, $this->bestSellersCollectionFactory);
            case "featured": return FeaturedProducts::class;
            case "mostviewed": return MostViewedProducts::class;
            case "onsale": return OnSaleProduct::class;
            case "recent": return RecentProducts::class;
            case "wishlist": return WishlistProducts::class;
            case "category": return CategoryId::class;
            case "custom": return CustomProducts::class;
            case "category-latest":;
        endswitch;
    }
}